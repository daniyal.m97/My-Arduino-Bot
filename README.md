A robot model for the following applications implemented individually.
1. Path Tracing
2. Obstacle Avoidance
3. Wireless Control

Components used:
1. Arduino Uno R3
2. L293D Motor driver
3. HC-06 bluetooth
4. HC-SR04 ultrasonic sensor
5. IR sensor
6. 5v BO geared motor
7. DIY 4-cell 5v battery pack
8. Chassis, wheels and screws to hold everything in place 

Dependencies:
1. AFMotor library 

External Dependencies:

Arduino Bluetooth Controller app (or any other BT terminal app, but the UI on this is nice)

Link - https://play.google.com/store/apps/details?id=com.broxcode.arduinobluetoothfree


