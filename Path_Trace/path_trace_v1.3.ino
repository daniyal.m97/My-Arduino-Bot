/*------ Program for Line Follower Robot using Arduino----- */
/*-------definning Inputs------*/
#define LS A0      // left sensor
#define RS A1      // right sensor
/*-------definning Outputs------*/
#define LM1 7       // left motor
#define LM2 8       // left motor
#define RM1 4       // right motor
#define RM2 5       // right motor
#define spd 120    // speed
void setup()
{
  pinMode(LS, INPUT);
  pinMode(RS, INPUT);
  pinMode(LM1, OUTPUT);
  pinMode(LM2, OUTPUT);
  pinMode(RM1, OUTPUT);
  pinMode(RM2, OUTPUT);
  pinMode(9,OUTPUT);
  pinMode(6,OUTPUT);
  digitalWrite(9,HIGH);
  digitalWrite(6,HIGH);
  }
void loop()
{
  if(!(digitalRead(LS)) && !(digitalRead(RS)))     // Move Forward
  {
    analogWrite(LM1, spd);
    digitalWrite(LM2, LOW);
    analogWrite(RM1, spd);
    digitalWrite(RM2, LOW);
  }
  
  if(digitalRead(LS) && !(digitalRead(RS)))     // Turn right
  {
    digitalWrite(LM1, LOW);
    digitalWrite(LM2, LOW);
    analogWrite(RM1, spd);
    digitalWrite(RM2, LOW);
  }
  
  if(!(digitalRead(LS)) && digitalRead(RS))     // turn left
  {
    analogWrite(LM1, spd);
    digitalWrite(LM2, LOW);
    digitalWrite(RM1, LOW);
    digitalWrite(RM2, LOW);
  }
  
  if(digitalRead(LS) && digitalRead(RS))     // stop
  {
    digitalWrite(LM1, LOW);
    digitalWrite(LM2, LOW);
    digitalWrite(RM1, LOW);
    digitalWrite(RM2, LOW);
  }
}
