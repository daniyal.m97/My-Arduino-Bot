/*------ Program for Bluetooth Robot using Arduino----- */
/*------App to control robot------*/
// https://play.google.com/store/apps/details?id=com.broxcode.arduinobluetoothfree
// Initiate buttons as respective states
/*-------definning Outputs------*/
#define LM1 7       // left motor
#define LM2 8       // left motor
#define RM1 4       // right motor
#define RM2 5       // right motor
#define spd 170     // speed (0-255)
int state=0;
int flag=0;
void setup()
{
  pinMode(LM1, OUTPUT);
  pinMode(LM2, OUTPUT);
  pinMode(RM1, OUTPUT);
  pinMode(RM2, OUTPUT);
  pinMode(9,OUTPUT);
  pinMode(6,OUTPUT);
  digitalWrite(9,HIGH);
  digitalWrite(6,HIGH);
  Serial.begin(38400);
  }
void loop()
{
  if(Serial.available()>0)
  {
   state=Serial.read(); 
   flag=0;
  }
  if(state=='1')     // Move Forward
  {
    analogWrite(LM1, spd);
    digitalWrite(LM2, LOW);
    analogWrite(RM1, spd);
    digitalWrite(RM2, LOW);
  }
  
  if(state=='2')     // Turn right
  {
    digitalWrite(LM1, LOW);
    digitalWrite(LM2, LOW);
    analogWrite(RM1, spd);
    digitalWrite(RM2, LOW);
  }
  
  if(state=='3')     // turn left
  {
    analogWrite(LM1, spd);
    digitalWrite(LM2, LOW);
    digitalWrite(RM1, LOW);
    digitalWrite(RM2, LOW);
  }
  
  if(state=='4')     // Move Backward
  {
    digitalWrite(LM1, LOW);
    analogWrite(LM2, spd);
    digitalWrite(RM1, LOW);
    analogWrite(RM2, spd);
  }
  if(state=='5')     // Stop
  {
    digitalWrite(LM1, LOW);
    digitalWrite(LM2, LOW);
    digitalWrite(RM1, LOW);
    digitalWrite(RM2, LOW);
  }
 flag=1;
}
